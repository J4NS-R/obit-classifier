
#ifndef OBIT_CLASSIFIER_TREEVISITORTESTSUITE_H
#define OBIT_CLASSIFIER_TREEVISITORTESTSUITE_H

#include "../../src/data/tree/TreeVisitor.h"
#include "../../src/data/tree/Node.h"
#include <gtest/gtest.h>
#include <vector>
#include <functional>

using namespace std;

class TreeVisitorTestSuite : public ::testing::Test{
public:
    TreeVisitor<const char> *visitor;
    static const std::vector<const char*> expected; // defined in cpp file

    void SetUp() override{
        auto* root = new Node(expected[0]);
        auto* child1 = new Node<const char>(expected[1]);
        child1->getChildren().push_back(new Node<const char>(expected[2]));
        root->getChildren().push_back(child1);
        root->getChildren().push_back(new Node<const char>(expected[3]));

        visitor = new TreeVisitor(root);
    }
};


#endif //OBIT_CLASSIFIER_TREEVISITORTESTSUITE_H
