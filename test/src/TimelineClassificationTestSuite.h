
#ifndef OBIT_CLASSIFIER_TIMELINECLASSIFICATIONTESTSUITE_H
#define OBIT_CLASSIFIER_TIMELINECLASSIFICATIONTESTSUITE_H

#include <gtest/gtest.h>
#include "../../src/owl/SOFITParser.h"
#include "../../src/config/ConfigParser.h"
#include "../../src/owl/ThreatClassifier.h"
#include "../../src/owl/TimelineParser.h"

class TimelineClassificationTestSuite : public ::testing::Test {
public:
    SOFITParser* sofit;
    ThreatClassifier* classifier;
    TimelineParser* timeline;

    void SetUp() override{
        sofit = new SOFITParser("test/res/empl5.sofit.owl");
        ConfigParser cfg(sofit);
        cfg.parseFile("test/res/config2.yml");
        classifier = new ThreatClassifier(sofit, cfg);

        timeline = new TimelineParser(sofit, "test/res/timeline.csv");
        ASSERT_FALSE(timeline->isError()) << timeline->getResetError();
    }
};


#endif //OBIT_CLASSIFIER_TIMELINECLASSIFICATIONTESTSUITE_H
