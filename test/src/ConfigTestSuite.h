
#ifndef OBIT_CLASSIFIER_CONFIGTESTSUITE_H
#define OBIT_CLASSIFIER_CONFIGTESTSUITE_H

#include <gtest/gtest.h>
#include "../../src/config/ConfigParser.h"
#include "../../src/owl/SOFITParser.h"

class ConfigTestSuite : public ::testing::Test{
public:
    SOFITParser* sofit;
    ConfigParser* config;

    void SetUp() override {
        sofit = new SOFITParser("test/res/empl5.sofit.owl");
        config = new ConfigParser(sofit);
        config->parseFile("test/res/config.yml");
        ASSERT_FALSE(config->isError()) << config->getResetError();
    }

};


#endif //OBIT_CLASSIFIER_CONFIGTESTSUITE_H
