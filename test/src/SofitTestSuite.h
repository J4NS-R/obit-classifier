#ifndef OBIT_CLASSIFIER_SOFITTESTSUITE_H
#define OBIT_CLASSIFIER_SOFITTESTSUITE_H

#include <gtest/gtest.h>
#include "../../src/owl/SOFITParser.h"

class SofitTestSuite : public ::testing::Test {
public:
    SOFITParser parser;
    explicit SofitTestSuite() : parser("test/res/empl5.sofit.owl"){
    };
    void SetUp() override{
        ASSERT_FALSE(parser.isError()) << parser.getResetError();
    }
};

#endif //OBIT_CLASSIFIER_SOFITTESTSUITE_H
