
#include <gtest/gtest.h>
#include "../../src/util/Date.h"
#include <list>

TEST(DateTests, comparisonTest){
    Date first("2021-01-01");
    Date mid1("2021-06-12");
    Date mid2("2021-06-12");
    Date end("2021-12-31");

    EXPECT_GT(mid1, first);
    EXPECT_LT(mid2, end);
    EXPECT_EQ(mid1, mid2);
    EXPECT_LT(first, end);
}

TEST(DateTests, formattingTest){
    std::list<string> dateStrs{
        "2021-01-05", "1998-04-23", "2000-11-11"
    };

    for(auto& dateStr : dateStrs){
        Date dateObj(dateStr);
        EXPECT_EQ(dateStr, dateObj.toDateStr());
    }
}
