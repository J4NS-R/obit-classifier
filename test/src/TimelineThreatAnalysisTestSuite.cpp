
#include "TimelineThreatAnalysisTestSuite.h"
#include "../../src/data/sofit/TimelineThreatAnalysis.h"
#include "../../src/config/ConfigParser.h"

TEST_F(TimelineThreatAnalysisTestSuite, timelineUpdateTest){
    TimelineThreatAnalysis anal("#Empl");
    EXPECT_EQ("#Empl", anal.getEmployee());
    EXPECT_EQ(0, anal.getRiskTotal());
    EXPECT_EQ(0, anal.getDurationDays());

    auto* fact1 = sofit->getFactor("#Reconnaissance");
    Date d1("2021-01-01");
    anal.addIncrementFactor(d1, fact1);
    EXPECT_EQ(71, anal.getRiskTotal());
    EXPECT_EQ(1, anal.getDurationDays());

    auto* fact2 = sofit->getFactor("#Aggressive_Outbursts");
    Date d2("2021-01-11");
    anal.decrementRisk(10); // days passed between d1 and d2
    EXPECT_EQ(61, anal.getRiskTotal());

    anal.addIncrementFactor(d2, fact2);
    EXPECT_EQ(71-10+63, anal.getRiskTotal());

    EXPECT_EQ(10, anal.getDurationDays());

    auto incs = anal.getIncidents();
    EXPECT_EQ(2, incs.size());
    auto inc_it = incs.begin();
    EXPECT_EQ(d1, inc_it->first);
    EXPECT_EQ(*fact1, *inc_it->second);

    inc_it++;
    EXPECT_EQ(d2, inc_it->first);
    EXPECT_EQ(*fact2, *inc_it->second);
}

TEST_F(TimelineThreatAnalysisTestSuite, minimumTest){
    TimelineThreatAnalysis anal("#Employee");
    ConfigParser config(sofit);
    config.parseFile("test/res/config-enduring.yml");
    ASSERT_FALSE(config.isError()) << config.getResetError();

    anal.addIncrementFactor(*new Date("2000-01-1"), sofit->getFactor("#Reconnaissance"));
    anal.decrementRisk(100);
    EXPECT_EQ(0, anal.getRiskTotal()) << "Non-enduring factors should be zero-able.";

    anal.addIncrementFactor(*new Date("2001-01-01"), sofit->getFactor("#Active_Foreign_Passport"));
    anal.decrementRisk(100);
    EXPECT_EQ(66, anal.getRiskTotal()) << "Enduring factor should set new minimum.";

    anal.addIncrementFactor(*new Date("2002-01-01"), sofit->getFactor("#Retiring"));
    anal.decrementRisk(100);
    EXPECT_EQ(66, anal.getRiskTotal()) << "Non-enduring factors should not change minimum.";
}
