
#include "TimelineParserTestSuite.h"

TEST_F(TimelineParserTestSuite, timelineDataTest){
    auto data = parser->getTimelineData();
    EXPECT_EQ(5, data.size()) << "Number of dates in timeline";

    Date twelfth("2021-01-12");
    ASSERT_TRUE(data.contains(twelfth));

    auto incidents = data.at(twelfth);
    ASSERT_EQ(2, incidents.size()) << "Number of incidents on 2021-01-12";

    auto incident = incidents.begin();
    EXPECT_EQ("#Jackson", incident->first);
    EXPECT_EQ("#Online_Shopping_Or_Gambling", incident->second->getIri());

    incident++;
    EXPECT_EQ("#Meyer", incident->first);
    EXPECT_EQ("#Performance_Below_Expectation", incident->second->getIri());
}

TEST_F(TimelineParserTestSuite, timelineByEmployeeTest){
    auto empls = parser->getTimelineByEmployee();
    EXPECT_EQ(3, empls.size());

    ASSERT_TRUE(empls.contains("#Meyer"));
    auto meyer = empls.at("#Meyer");
    EXPECT_EQ(3, meyer.size()) << "Number of incidents for #Meyer";

    auto date_it = meyer.begin();
    EXPECT_EQ(*new Date("2021-01-01"), date_it->first);
    EXPECT_EQ("#Job_Search", date_it->second->getIri());

    date_it++; date_it++;  // must manually advance twice
    EXPECT_EQ(*new Date("2021-01-12"), date_it->first);
    EXPECT_EQ("#Performance_Below_Expectation", date_it->second->getIri());

}
