
#ifndef OBIT_CLASSIFIER_TIMELINETHREATANALYSISTESTSUITE_H
#define OBIT_CLASSIFIER_TIMELINETHREATANALYSISTESTSUITE_H

#include <gtest/gtest.h>
#include "../../src/owl/SOFITParser.h"

class TimelineThreatAnalysisTestSuite : public ::testing::Test{
public:
    SOFITParser* sofit;

    void SetUp() override{
        sofit = new SOFITParser("sofit.owl");
        ASSERT_FALSE(sofit->isError()) << sofit->getResetError();
    }

};


#endif //OBIT_CLASSIFIER_TIMELINETHREATANALYSISTESTSUITE_H
