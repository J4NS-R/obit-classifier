
#include "ConfigTestSuite.h"
#include <gtest/gtest.h>

TEST_F(ConfigTestSuite, configTest){
    auto factorMap = sofit->getFactorRisks();

    ASSERT_TRUE(factorMap.contains("#Extreme_Discontent"));
    EXPECT_EQ(5, factorMap.at("#Extreme_Discontent"));

    ASSERT_TRUE(factorMap.contains("#Attendance_Issues"));
    EXPECT_EQ(16, factorMap.at("#Attendance_Issues"));

    ASSERT_TRUE(factorMap.contains("#Absences"));
    EXPECT_EQ(17, factorMap.at("#Absences"));

    ASSERT_TRUE(factorMap.contains("#Early_Departures"));
    EXPECT_EQ(16, factorMap.at("#Early_Departures"));
}

TEST_F(ConfigTestSuite, unassignedTest){
    config->parseFile("test/res/config-unassigned.yml");
    ASSERT_FALSE(config->isError()) << config->getResetError();
    auto factorMap = sofit->getFactorRisks();

    ASSERT_TRUE(factorMap.contains("#Factor")) << "#Factor missing from risk-map after default override.";
    EXPECT_EQ(25, factorMap.at("#Factor")) << "Bad post-default-override value for #Factor.";

    ASSERT_TRUE(factorMap.contains("#Work_Role_Ambiguity")) << "#Work_Role_Ambiguity missing from risk-map after default override.";
    EXPECT_EQ(25, factorMap.at("#Work_Role_Ambiguity")) << "Bad post-default-override value for #Work_Role_Ambiguity.";
}

TEST_F(ConfigTestSuite, cooldownTest){
    config->parseFile("test/res/config2.yml");
    ASSERT_FALSE(config->isError()) << config->getResetError();

    EXPECT_TRUE(config->isCooldownSpecified());
    EXPECT_EQ(2.8, config->getCooldown());
}

TEST_F(ConfigTestSuite, matrixTest){
    config->parseFile("test/res/config2.yml");
    ASSERT_FALSE(config->isError()) << config->getResetError();

    EXPECT_TRUE(config->isClassMatrixSpecified());
    auto pMatrix = config->getClassificationMatrix();

    EXPECT_EQ(0, *(pMatrix));
    EXPECT_EQ(50, *(pMatrix+1));
    EXPECT_EQ(85, *(pMatrix+2));
    EXPECT_EQ(120, *(pMatrix+3));
}

TEST_F(ConfigTestSuite, runToTodayTest){
    config->parseFile("test/res/config-today.yml");
    ASSERT_FALSE(config->isError()) << config->getResetError();

    EXPECT_TRUE(config->getRunToToday());
}

TEST_F(ConfigTestSuite, enduringFactorsTest){
    config->parseFile("test/res/config-enduring.yml");
    ASSERT_FALSE(config->isError()) << config->getResetError();

    auto* fact = sofit->getFactor("#New-Hire");
    EXPECT_FALSE(fact->isEnduring()) << "#New_Hire should be unaffected";

    fact = sofit->getFactor("#Ideology");
    EXPECT_TRUE(fact->isEnduring()) << "#Ideology was explicitly set to enduring";

    fact = sofit->getFactor("#Disloyal_Connections");
    EXPECT_TRUE(fact->isEnduring()) << "#Disloyal_Connections should be enduring (recursive defn)";

}
