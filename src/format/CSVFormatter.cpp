
#include "CSVFormatter.h"
#include <iostream>

using namespace std;

void CSVFormatter::formatAndPrint() {
    // print headers
    int i = 0;
    for(auto& header : headers){
        cout << header;
        if (i++ < headers.size()-1)
            cout << ",";  // skip comma at end
    }
    cout << endl;

    // print data
    for(auto& row : data){
        i = 0;
        for(auto& el : row){
            cout << el;
            if (i++ < row.size()-1)
                cout << ",";
        }
        cout << endl;
    }

}
