
#ifndef OBIT_CLASSIFIER_THREATCLASSIFIER_H
#define OBIT_CLASSIFIER_THREATCLASSIFIER_H

#include <map>
#include <string>
#include "SOFITParser.h"
#include "../data/sofit/ThreatAnalysis.h"
#include "../config/ConfigParser.h"
#include "../data/sofit/TimelineThreatAnalysis.h"

using namespace std;
typedef ThreatAnalysis::THREAT_LEVEL THREAT_LEVEL;

class ThreatClassifier {
public:
    constexpr static const int CLASS_MATRIX_LEN = 4;

private:
    // internal static definitions
    constexpr static const int defaultClassMatrix[] = {0, 25, 75, 100};
    constexpr static const double defaultCooldown = 1;

    // members
    const SOFITParser* sofit;

    // The 4 ints refer to the max (inclusive) risk total for the relevant threat level
    int classificationMatrix[CLASS_MATRIX_LEN];
    double dailyCooldown;
    map<string, ThreatAnalysis*> emplClassifications;
    bool runToToday;

public:
    /**
     * Initialises the class and sets up internal config params
     */
    explicit ThreatClassifier(const SOFITParser* sofit, const ConfigParser& config) :
        sofit(sofit){

        const int* p_useMatrix;
        if (config.isClassMatrixSpecified()){
            p_useMatrix = config.getClassificationMatrix();
        }else{
            p_useMatrix = defaultClassMatrix;
        }

        // set classification matrix
        for(int i = 0; i < CLASS_MATRIX_LEN; i++){
            classificationMatrix[i] = *(p_useMatrix+i);
        }

        // set cooldown
        if (config.isCooldownSpecified())
            dailyCooldown = config.getCooldown();
        else
            dailyCooldown = defaultCooldown;

        // run to today
        runToToday = config.getRunToToday();
    }

    // external utility funcs (documentation in .cpp file)
    void classifyThreats();
    TimelineThreatAnalysis classifyTimeline(const string& empl, list<pair<Date, const Factor*>> dateFactors);
    void classifyRunningTimeline(TimelineThreatAnalysis* anal, Date the_date, const Factor* fact, int daysSinceLast=0);
    void classifyToToday(TimelineThreatAnalysis* anal, Date& lastIncident);

    // getters
    [[nodiscard]] const map<string, ThreatAnalysis*> &getEmplClassifications() const {
        return emplClassifications;
    }

private:
    // internal utility functions
    THREAT_LEVEL classifyRisk(double riskVal);

};


#endif //OBIT_CLASSIFIER_THREATCLASSIFIER_H
