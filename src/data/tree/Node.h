#ifndef OBIT_CLASSIFIER_NODE_H
#define OBIT_CLASSIFIER_NODE_H

#include <list>

/**
 * A dumb tree node
 * @tparam T type of data this node holds
 */
template<class T>
class Node {
private:
    T* data;
    std::list<Node<T>*> children{};
public:
    explicit Node(T* data){
        this->data = data;
    }

    std::list<Node<T>*> &getChildren() {
        return children;
    }

    T* getData(){  // mutable
        return data;
    }

    // Node shall be responsible for destroying its children
    ~Node(){
        for(auto &it : children){
            delete it; //dereferencing the iterator fetches a reference to the element
        }

        // don't try and delete consts
        if (!std::is_const<T>::value)
            delete data;
    }
};

#endif
