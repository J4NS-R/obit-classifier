
#ifndef OBIT_CLASSIFIER_UTIL_H
#define OBIT_CLASSIFIER_UTIL_H

#include <map>
#include <string>
#include <yaml-cpp/yaml.h>
#include <vector>
#include "Date.h"

template<typename K, typename V>
static V getOrDefault(std::map<K,V> map, K lookup, V def){
    if (map.contains(lookup))
        return map[lookup];
    else
        return def;
}

/**
 * Split a string by a delimiter, and return a list of parts.
 * Note that the vector may contain empty items.
 */
static std::vector<string> strsplit(std::string& subject, char delim){
    std::istringstream iss(subject);
    std::vector<string> vec;
    std::string token;
    while(std::getline(iss, token, delim)){
        vec.push_back(token);
    }
    return vec;
}

// only positive integers pass.
static bool isNumber(std::string& str){
    for(const char& c : str){
        // 0 means not in class (digits)
        if (std::isdigit(c)==0) return false;
    }
    return true;
}

static bool yaml_isNumber(YAML::Node& node){
    if (!node.IsScalar())
        return false;
    auto str = node.as<std::string>();
    return isNumber(str);
}

/**
 * Checks whether a string is a positive floating point or integer representation. Decimal is a dot, not comma.
 */
static bool isFloat(std::string& str){
    bool dot = false;
    for(const char& c : str){
        if (std::isdigit(c)==0) { // not a digit
            if (c == '.'){
                if (dot) // already has a dot in expression
                    return false;
                else dot = true;
            }else
                return false;
        }// else digit -- continue
    }
    return true;
}

static bool yaml_isFloat(YAML::Node& node){
    if (!node.IsScalar())
        return false;
    auto str = node.as<std::string>();
    return isFloat(str);
}

static bool yaml_isBool(YAML::Node& node){
    if (!node.IsScalar())
        return false;
    auto str = node.as<std::string>();
    return  // only these 6 options are valid. "TRue" is not a valid boolean in YAML.
        str == "true" || str == "True" || str == "TRUE" ||
        str == "false" || str == "False" || str == "FALSE";
}

/**
 * Check if a list contains a factor, by IRI
 */
static bool containsFactor(const list<const Factor*>& factorList, const string& targetFactor){
    return std::any_of(factorList.begin(), factorList.end(), [targetFactor](const Factor* factor){
        return factor->getIri() == targetFactor;
    });
}

/**
 * Calculates the difference between two Dates in days.
 * Assumes first is before second.
 * Truncates excess time less than a day.
 */
static int dayDiff(const Date& first, const Date& second){
    double secs = first.diffSecs(second);
    int daySecs = 60*60*24;
    int days = 0;
    while (secs >= daySecs){
        secs -= daySecs;
        days++;
    }
    return days;
}

static string prettifyDuration(int days){
    if (days <= 0)
        return "0 days";

    string output;
    int years = 0;
    while(days >= 365){  // I know mod exists, but this is the cleanest way to get both
        days-= 365;      // the int and remainder.
        years++;
    }
    if (years > 0){
        output.append(to_string(years)).append(" year");
        if (years > 1)
            output.append("s");
        output.append(" ");
    }
    int months = 0;
    while(days >= 30){
        days -= 30;
        months++;
    }
    if (months > 0){
        output.append(to_string(months)).append(" month");
        if (months > 1)
            output.append("s");
        output.append(" ");
    }
    if (days > 0){
        output.append(to_string(days)).append(" day");
        if (days > 1)
            output.append("s");
    }
    return output;
}

template<typename T>
static decltype(auto) convert_to_c(const T& x){
    return x; // base case - change nothing
}

static decltype(auto) convert_to_c(const std::string& str){
    return str.c_str();
}

/**
 * Credit to https://stackoverflow.com/a/36909699/3900981 and https://stackoverflow.com/a/51856545/3900981
 * For concocting this C++ version of sprintf()
 */
template<typename... Args>
static string stringprintf(const char* format, Args&&... args){
    auto size = snprintf(nullptr, 0, format, convert_to_c(args)...);
    char buffer[size];
    snprintf(&buffer[0], size, format, convert_to_c(args)...);
    return string(buffer);
}

#endif
