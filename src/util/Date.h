
#ifndef OBIT_CLASSIFIER_DATE_H
#define OBIT_CLASSIFIER_DATE_H

#include <ctime>
#include <string>
#include <sstream>
#include <iomanip>

using namespace std;

/**
 * Wraps ctime, and provides convenient string parsing and comparison operators
 */
class Date {
private:
    struct tm the_time = {};
    time_t secs = -1;

    static string zeroPad(int figure){
        string conv = to_string(figure);
        if (conv.size() == 1)
            conv = "0"+conv;
        return conv;
    }

public:
    explicit Date(const string& dateStr){
        stringstream rawDateStream(dateStr);
        rawDateStream >> std::get_time(&the_time, "%Y-%m-%d");  //ISO8601
        secs = std::mktime(&the_time);
    }
    // default constructor uses today's date
    explicit Date(){
        auto now = time(nullptr);
        tm* timenow = localtime(&now);
        the_time = *timenow;
        secs = std::mktime(&the_time);
    }

    [[nodiscard]] time_t getSecs() const{
        return secs;
    }
    [[nodiscard]] string toDateStr() const{
        string output;
        output.append(to_string(1900+the_time.tm_year))
            .append("-").append(zeroPad(1+the_time.tm_mon))
            .append("-").append(zeroPad(the_time.tm_mday));
        return output;
    }

    // seconds from this time to otherDate's time
    [[nodiscard]] double diffSecs(const Date& otherDate) const{
        // otherDate is end, and this is start
        return std::difftime(otherDate.secs, secs);
    }
    bool operator< (const Date& otherDate) const{
        return diffSecs(otherDate) > 0;
        // if positive then this time is before otherDate's time.
    }
    bool operator> (const Date& otherDate) const{
        return diffSecs(otherDate) < 0;
        // if negative then this time is after otherDate's time.
    }
    bool operator== (const Date& otherDate) const{
        return diffSecs(otherDate) == 0;
    }
    bool operator>= (const Date& otherDate) const{
        return *this>otherDate || *this==otherDate;
    }
    bool operator<= (const Date& otherDate) const{
        return *this<otherDate || *this==otherDate;
    }

};


#endif //OBIT_CLASSIFIER_DATE_H
