
#ifndef OBIT_CLASSIFIER_CONFIGPARSER_H
#define OBIT_CLASSIFIER_CONFIGPARSER_H

#include <string>
#include <map>
#include "../owl/SOFITParser.h"

using namespace std;

class ConfigParser : public ErrorProne {
private:
    SOFITParser* sofit;  // todo change to reference
    int classificationMatrix[4] = {-1,};
    double cooldown = -1;
    bool runToToday = false;  // default value

public:
    /**
     * Creates an empty config.
     * @param sofit parsed SOFIT ontology.
     */
    explicit ConfigParser(SOFITParser* sofit){
        this->sofit = sofit;
    };

    /**
     * Parse a config yaml file.
     * Check getError() afterwards.
     * @param infile
     */
    void parseFile(const string& infile);

    // getters
    /**
     * @return true if parsed config specifies a valid custom threat classification matrix. False otherwise.
     */
    [[nodiscard]] bool isClassMatrixSpecified() const{
        return classificationMatrix[0] > -1;
    }

    [[nodiscard]] bool isCooldownSpecified() const{
        return cooldown > -1;
    }

    /**
     * Check isClassMatrixSpecified() first.
     * @return the specified classification matrix (int[] of length 4). Will be garbage if user did not specify one.
     */
    [[nodiscard]] const int* getClassificationMatrix() const {
        return classificationMatrix;
    }

    [[nodiscard]] double getCooldown() const{
        return cooldown;
    }

    [[nodiscard]] bool getRunToToday() const{
        return runToToday;
    }
};


#endif //OBIT_CLASSIFIER_CONFIGPARSER_H
