
#ifndef OBIT_CLASSIFIER_TIMELINECOMMAND_H
#define OBIT_CLASSIFIER_TIMELINECOMMAND_H

#include "../owl/TimelineParser.h"

class TimelineCommand {
private:
    TimelineParser* timeline;
    ThreatClassifier classifier;
    bool runToToday;

public:
    explicit TimelineCommand(TimelineParser* timeline, SOFITParser* sofit, ConfigParser& cfg) :
    timeline(timeline), classifier(sofit, cfg){
        runToToday = cfg.getRunToToday();
    }

    void executeAndPrint(const string& format);
};


#endif //OBIT_CLASSIFIER_TIMELINECOMMAND_H
