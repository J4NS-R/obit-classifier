#include <iostream>
#include "src/owl/SOFITParser.h"
#include "src/config/ConfigParser.h"
#include "src/owl/EmployeeParser.h"
#include "src/command/PrintEmployeesCommand.h"
#include "src/command/ClassifyCommand.h"
#include "src/command/ClassifyTimelineCommand.h"
#include "src/command/TimelineCommand.h"
#include <boost/program_options.hpp>
#include <string>
#include <list>

namespace po = boost::program_options;
using namespace std;

int main(int argc, const char* argv[]) {

    // declare options
    po::options_description desc("Command-line options");
    desc.add_options()  // get a load of this syntax. Is this really C++?
            ("help,?", "Print help.")
            ("ontology,o", po::value<string>()->required(), "[Required] SOFIT OWL file (may include employee instances)")
            ("employees,e", po::value<string>(), "Employee CSV file")
            ("config,c", po::value<string>(), "Configuration YAML file")
            ("format,f", po::value<string>()->default_value("table"), "Format to output in. Only applies to certain commands.")
            ("timeline,t", po::value<string>(), "Timeline CSV file. Required for timeline reconstruction functionality.")
            ("command", po::value<string>()->required(), "Command to execute. Can be passed as an arg.")
    ;

    po::positional_options_description pos_opts;
    pos_opts.add("command", 1);
    list<pair<string,string>> valid_commands{
        pair("print-tree", "Prints the SOFIT tree (including config mutations) (ignores -f)"),
        pair("print-employees", "Prints all employees and their factors (obeys -f)"),
        pair("classify", "Classify all employees into threat levels (obeys -f)"),
        pair("classify-timeline", "Classify employees at present, considering past timeline (requires -t; obeys -f)"),
        pair("timeline", "Reconstruct a timeline of insider behaviour (requires -t; obeys -f)"),
    };
    list<pair<string,string>> valid_formats{
        pair("table", "A clean table (default)."),
        pair("json", "JSON format"),
        pair("csv", "CSV format (with headers)"),
    };

    // parse command-line options
    po::variables_map argMap;
    po::store(po::command_line_parser(argc, argv).options(desc).positional(pos_opts).run(), argMap);

    // display help and stop
    if (argMap.count("help")){
        cout << desc << endl << "Valid commands: " << endl;
        for(auto& cmdPair : valid_commands){
            cout << "  " << cmdPair.first << "  -  " << cmdPair.second << endl;
        }
        cout << endl << "Valid formats (-f):" << endl;
        for(auto& formPair : valid_formats){
            cout << "  " << formPair.first << "  -  " << formPair.second << endl;
        }
        return 0;
    }

    // throw errors if necessary
    po::notify(argMap);

    // check if command supported
    string command = argMap["command"].as<string>();
    bool commandLegit = false;
    for(auto& cmdPair : valid_commands){
        if (command == cmdPair.first){
            commandLegit = true;
            break;
        }
    }
    if (!commandLegit){
        cout << "Command \"" << command << "\" not recognised. Run with --help to see valid commands." << endl;
        return 1;
    }

    // check if format valid
    string format = argMap["format"].as<string>();
    bool formatLegit = false;
    for (auto& frmPair : valid_formats){
        if (format == frmPair.first){
            formatLegit = true;
            break;
        }
    }
    if (!formatLegit){
        cout << "Format \"" << format << "\" not recognised. Run with --help to see valid formats." << endl;
        return 1;
    }

    // parse SOFIT (ontology is a required option)
    SOFITParser sofit(argMap["ontology"].as<string>().c_str());

    if (sofit.isError()){
        cout << "Failed to parse OWL file: " << sofit.getResetError() << endl;
        return 1;
    }

    // parse config (if any)
    ConfigParser config(&sofit);
    if (argMap.count("config")){
        config.parseFile(argMap["config"].as<string>());

        if (config.isError()){
            cout << "Error parsing config: " << config.getResetError() << endl;
            return 1;
        }
    }

    // parse extra employees (if any)
    EmployeeParser empls(&sofit);
    if (argMap.count("employees")){
        empls.parseFile(argMap["employees"].as<string>());

        if (empls.isError()){
            cout << "Error parsing employees CSV file: " << empls.getResetError() << endl;
            return 1;
        }
    }

    sofit.validate();
    if (sofit.isError()){
        cout << "Error in SOFIT: " << sofit.getResetError() << endl;
        return 1;
    }

    TimelineParser* timelineParser = nullptr;
    if (argMap.count("timeline")){
        timelineParser = new TimelineParser(&sofit, argMap.at("timeline").as<string>());
        if (timelineParser->isError()){
            cout << "Error parsing timeline: " << timelineParser->getResetError() << endl;
            return 1;
        }
    }

    // switch over command
    if (command == "print-tree"){
        sofit.printTree();  // ignore formatting

    }else if (command == "print-employees"){
        PrintEmployeesCommand cmd(&sofit);
        cmd.executeAndPrint(format);

    }else if (command == "classify"){
        ClassifyCommand cmd(&sofit, config);
        cmd.executeAndPrint(format);

    }else if (command == "classify-timeline"){
        if (timelineParser==nullptr){
            cout << "-t option is missing and required for classify-timeline command." << endl;
            return 1;
        }
        ClassifyTimelineCommand cmd(timelineParser, &sofit, config);
        cmd.executeAndPrint(format);

    }else if (command == "timeline"){
        if (timelineParser==nullptr){
            cout << "-t option is missing and required for timeline command." << endl;
            return 1;
        }
        TimelineCommand cmd(timelineParser, &sofit, config);
        cmd.executeAndPrint(format);

    }

    return 0;
}
